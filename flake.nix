# SPDX-FileCopyrightText: 2023 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "24.05";
    };

    flake-parts = {
      type = "github";
      owner = "hercules-ci";
      repo = "flake-parts";
    };

    pre-commit-hooks-nix = {
      type = "github";
      owner = "cachix";
      repo = "pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        nixpkgs-stable.follows = "nixpkgs";
      };
    };

    poetry2nix = {
      type = "github";
      owner = "nix-community";
      repo = "poetry2nix";
      ref = "master";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };

    leapseconds = {
      url = "https://data.iana.org/time-zones/tzdb/leap-seconds.list";
      flake = false;
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-parts,
    pre-commit-hooks-nix,
    poetry2nix,
    leapseconds,
    ...
  }:
    flake-parts.lib.mkFlake
    {inherit inputs;}
    ({...}: {
      imports = [
        pre-commit-hooks-nix.flakeModule
      ];

      config = {
        systems = nixpkgs.lib.systems.flakeExposed;

        perSystem = {
          system,
          config,
          pkgs,
          ...
        }: {
          _module.args.pkgs = import inputs.nixpkgs {
            inherit system;
            overlays = [
              poetry2nix.overlays.default
            ];
            config = {};
          };

          devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [poetry reuse];
            shellHook = ''
              ${config.pre-commit.installationScript}
            '';
          };

          pre-commit = {
            settings.hooks = {
              reuse = {
                enable = true;
                pass_filenames = false;
                entry = "${config.pre-commit.pkgs.reuse}/bin/reuse lint";
              };
              black.enable = true;
              alejandra.enable = true;
            };
          };

          packages = rec {
            default = ca;

            inherit leapseconds;

            ca = with pkgs.poetry2nix;
              mkPoetryApplication {
                projectDir = cleanPythonSources {src = ./.;};

                overrides = overrides.withDefaults (self: super: {
                  leapseconddata = super.leapseconddata.overridePythonAttrs (oldAttrs: {
                    buildInputs = (oldAttrs.buildInputs or []) ++ [self.setuptools];
                  });
                  cryptography = super.cryptography.override {preferWheel = true;};
                  pyyaml = super.pyyaml.override {preferWheel = true;};
                });

                nativeBuildInputs = with pkgs; [makeWrapper];

                postInstall = ''
                  wrapProgram $out/bin/ca \
                    --set-default LEAPSECONDS_FILE ${leapseconds} \
                    --prefix PATH : ${pkgs.lib.makeBinPath (with pkgs; [sops])}

                  wrapProgram $out/bin/tai64dec \
                    --set-default LEAPSECONDS_FILE ${leapseconds}
                '';

                meta.mainProgram = "ca";
              };
          };
        };
      };
    });
}
