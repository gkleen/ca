# SPDX-FileCopyrightText: 2023 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .lib import tai64int

__all__ = ["tai64int"]
