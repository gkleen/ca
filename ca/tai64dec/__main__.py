# SPDX-FileCopyrightText: 2023 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys, os

import argparse

from leapseconddata import LeapSecondData
from datetime import datetime, timezone
from .lib import tai64int


class BooleanAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        super(BooleanAction, self).__init__(option_strings, dest, nargs=0, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(
            namespace, self.dest, False if option_string.startswith("--no") else True
        )


def main():
    parser = argparse.ArgumentParser(
        prog="tai64dec", formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("--random", "--no-random", action=BooleanAction, default=False)
    parser.add_argument("--ns", "--no-ns", action=BooleanAction, default=True)
    args = parser.parse_args()

    out = tai64int(
        datetime.now(tz=timezone.utc),
        append_ns=args.ns,
        append_random=args.random,
    )

    print("{:d}".format(out), file=sys.stdout)


if __name__ == "__main__":
    sys.exit(main())
