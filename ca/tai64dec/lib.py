# SPDX-FileCopyrightText: 2023 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from leapseconddata import LeapSecondData
from math import ldexp
from pathlib import Path
import secrets


def tai64int(dt, append_ns=False, append_random=False):
    global leapsecond_data

    have_data = False
    try:
        have_data = bool(leapsecond_data)
    except NameError:
        pass

    if not have_data:
        leapsecond_data = LeapSecondData.from_file(Path(os.getenv("LEAPSECONDS_FILE")))

    tai_dt = leapsecond_data.to_tai(dt)
    seconds = int(tai_dt.timestamp())
    out = seconds + int(ldexp(1, 62))

    if append_ns:
        nanoseconds = int((tai_dt.timestamp() - seconds) / 1e-9)
        out = out << 32 | nanoseconds

    if append_random:
        out = out << 24 | int.from_bytes(
            secrets.token_bytes(3), byteorder="little", signed=False
        )

    return out
